from flask import render_template, request, current_app
from . import main
from ..models import Post

@main.route('/')
def index():
	page = request.args.get('page', 1, type = int)
	pagination = Post.query.order_by(Post.timestamp.desc()).paginate(page, per_page = current_app.config['BLOG_POSTS_PER_PAGE'], error_out = False)
	posts = pagination.items
	#posts = Post.query.order_by(Post.timestamp.desc()).all()
	return render_template('main/index.html', posts = posts, pagination = pagination, single_post = False)


@main.route('/post/<int:id>')
def post(id):
	post = Post.query.get_or_404(id)
	return render_template('main/post.html', post = post, single_post = True)
#coding=utf-8
from datetime import datetime
from flask.ext.login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from . import db, login_manager
from markdown import markdown
import bleach


"""
u = User(email='me@kweima.com', username='kweima', password='123')
"""


class User(UserMixin, db.Model):

	__tablename__ = 'users'
	id = db.Column(db.Integer, primary_key = True)
	email = db.Column(db.String(64), unique = True, index = True)
	username = db.Column(db.String(64), unique = True, index = True)
	password_hash = db.Column(db.String(128))
	posts = db.relationship('Post', backref = 'author', lazy = 'dynamic')

	# 创建只写属性
	@property
	def password(self):
		raise AttributeError('<password> is not a readable attribute!')

	# 写密码时生成哈希值
	@password.setter
	def password(self, password):
		self.password_hash = generate_password_hash(password)

	# 验证密码
	def verify_password(self, password):
		return check_password_hash(self.password_hash, password)


class Post(db.Model):

	__tablename__ = 'posts'
	id = db.Column(db.Integer, primary_key = True)
	title = db.Column(db.String(128), nullable = False)
	content = db.Column(db.Text)
	content_html = db.Column(db.Text)
	excerpt = db.Column(db.Text)
	status = db.Column(db.String(64))
	timestamp = db.Column(db.DateTime, index = True, default = datetime.utcnow)
	author_id = db.Column(db.Integer, db.ForeignKey('users.id'))

	@staticmethod
	def on_changed_content(target, value, oldvalue, initiator):
		allowed_tags = ['a', 'abbr', 'acronym', 'b', 'blockquote', 'code',
                        'em', 'i', 'li', 'ol', 'pre', 'strong', 'ul',
                        'h1', 'h2', 'h3', 'h4', 'h5', 'p']
		target.content_html = bleach.linkify(bleach.clean(
        	markdown(value, output_format='html'), 
        	tags=allowed_tags, strip=True))


db.event.listen(Post.content, 'set', Post.on_changed_content)


@login_manager.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))

from flask.ext.wtf import Form
"""
StringField
TextAreaField
PasswordField
HiddenField
DateField
DateTimeField
IntegerField
DecimalField
FloadField
BooleanField
RadioField
SelectField
SelectMultipleField
FileField
SubmitField
FormField
FieldList
"""
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import Required, Length

class LoginForm(Form):
	username = StringField('Username', validators = [Required(), Length(1, 64)])
	password = PasswordField('Password', validators = [Required()])
	remember_me = BooleanField('Remember me')
	submit = SubmitField('Login')

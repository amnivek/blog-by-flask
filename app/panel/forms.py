from datetime import datetime
from flask.ext.wtf import Form
from wtforms import StringField, SubmitField, TextAreaField, DateTimeField, SelectField
from wtforms.validators import Required, Length
from flask.ext.pagedown.fields import PageDownField


class PostForm(Form):
	title = StringField('Title', validators = [Required(), Length(1, 128)])
	content = PageDownField('Content')
	# full format : %Y-%m-%dT%H:%M:%S.%LZ
	timestamp = DateTimeField('Date time', format = '%Y-%m-%d %H:%M:%S', default = datetime.utcnow())
	status = SelectField('Status', coerce=int, choices=[(1, 'Foo 1'), (2, 'Foo 2')])
	submit = SubmitField('Submit')
	save = SubmitField('Save')
	discard = SubmitField('Discard')
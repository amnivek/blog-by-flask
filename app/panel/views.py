from flask import render_template, flash, redirect, url_for, request, current_app
from . import panel
from flask.ext.login import login_required, current_user
from ..models import Post
from .forms import PostForm
from .. import db


@panel.route('/')
@login_required
def index():
	return render_template('panel/index.html')


@panel.route('/edit_post', methods = ['GET', 'POST'])
@login_required
def edit_post():
	form = PostForm()
	if form.validate_on_submit():
		post = Post(
			title = form.title.data,
			content = form.content.data,
			timestamp = form.timestamp.data,
			author_id = current_user.get_id(),
			author = current_user._get_current_object(),
			)
		db.session.add(post)
		return redirect(url_for('.index'))
		flash('Submited')
	return render_template('panel/edit_post.html', form = form)


@panel.route('/post_list', methods = ['GET', 'POST'])
@login_required
def post_list():
	if request.method == 'GET':
		page = request.args.get('page', 1, type = int)
		#pagination = Post.query.with_entities(Post.title, Post.timestamp).order_by(Post.timestamp.desc()).paginate(page, per_page = current_app.config['BLOG_POSTS_PER_PAGE'], error_out = False)
		pagination = Post.query.with_entities(Post.title, Post.timestamp).order_by(Post.timestamp.desc()).paginate(page, per_page = 1000, error_out = False)
		posts = pagination.items
		return render_template('panel/post_list.html', posts = posts, pagination = pagination, single_post = False)